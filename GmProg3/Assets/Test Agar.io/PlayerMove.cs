﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	public float playerspeed;
	public Camera playercamera;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 target = playercamera.ScreenToWorldPoint (Input.mousePosition);
		target.z = transform.position.z;

		transform.position = Vector3.MoveTowards (this.transform.position, target, (playerspeed * Time.deltaTime)/this.transform.localScale.x);
	
	}
}
