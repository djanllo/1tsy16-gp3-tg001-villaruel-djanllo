﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Gamemanager : MonoBehaviour {
	
	public enum celltype
	{
		verysmall = 10,
		small = 50,
		medium = 100,
		mediumlarge = 150,
		category1 = 200,
		category2 = 250,
		largecell = 300,
	};
	
	
	//public celltype Celltype;
	
	float initialcamsize;
	public GameObject playerparent;
	public float playerwalkspeed;
	private Vector3 playerdirection;
	public Vector3 playersize;
	public float playerturnSpeed;
	public Camera[] playercam;
	public float playermass;
	
	public Text playermasstext;
	
	
	// Use this for initialization
	void Start () {
		playersize = this.transform.localScale;
		playerparent.GetComponent<playersynch> ().addNewtransform (this.transform);
		//Celltype = celltype.verysmall;
	}
	
	// Update is called once per frame
	void Update () {

		
		transform.SetParent(playerparent.transform);
		
		//onscreen text
		playermasstext.text = "Mass: " + playermass;

		//wall restriction
		if (this.gameObject.transform.position.x > 100) 
		{
			this.gameObject.transform.position = new Vector3(100, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
		}
		
		if (this.gameObject.transform.position.x < -100) 
		{
			this.gameObject.transform.position = new Vector3(-100, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
		}
		
		if (this.gameObject.transform.position.y > 100) 
		{
			this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, 100, this.gameObject.transform.position.z);
		}
		
		if (this.gameObject.transform.position.y < -100) 
		{
			this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, -100, this.gameObject.transform.position.z);
		}
	}
	
	void LateUpdate()
	{
		initialcamsize = 3.5f + playermass/50;
		
		//makes the camera follow the player
		for (int i = 0; i < playercam.Length; i++) 
		{
			playercam [i].transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, -10f);
		}
		
		//camera zoom
		if (playercam [0].orthographicSize < initialcamsize) 
		{
			playercam [0].orthographicSize = initialcamsize;
		}
	}
	
	public void updatesize(float addedsize, float addedmass)
	{
		//makes the player larger
		this.transform.localScale += new Vector3 (addedsize,addedsize,addedsize);
		playersize = this.transform.localScale;
		playermass += addedmass;
		this.GetComponent<Rigidbody2D> ().mass = playermass;
		
	}
	
	public void split()
	{
		//if playermass is more than 35 you can split the cell
		if (playermass > 35) 
		{
			playermass = Mathf.Round(playermass/2);
			this.transform.localScale = this.transform.localScale/2;
			playersize = this.transform.localScale/2;
			GameObject splittedplayer;
			splittedplayer = this.gameObject;
			Vector3 target = Camera.main.ScreenToWorldPoint( Input.mousePosition );
			target.z = -1;
			Instantiate(splittedplayer, target, Quaternion.identity);
		}
	}
	
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "drop") 
		{
			//calls the function to make the player grow
			updatesize(0.2f, 1);
			
			Debug.Log(col.tag);
			//destroy collided object
			Destroy(col.gameObject);
		}
		
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player") 
		{
			if(col.transform.localScale.x < this.transform.localScale.x)
			{
				//calls the function to make the player grow
				updatesize(col.transform.localScale.x, col.transform.GetComponent<Gamemanager>().playermass);
				
				Debug.Log(col.tag);
				//destroy collided object
				Destroy(col.gameObject);
			}
		}
	}
}
