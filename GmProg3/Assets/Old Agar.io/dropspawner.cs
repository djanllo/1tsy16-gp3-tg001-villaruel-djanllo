﻿using UnityEngine;
using System.Collections;

public class dropspawner : MonoBehaviour {

	public float Minspawnrangex;
	public float Maxspawnrangex;
	public float Minspawnrangey;
	public float Maxspawnrangey;

	public float spawnrate;
	public float spawnlimit;

	public GameObject pickups;
	public float time;
	public float NumberOfPickupsInTheField;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		NumberOfPickupsInTheField = GameObject.FindGameObjectsWithTag ("drop").Length;
		if (time >= spawnrate) 
		{
			if(GameObject.FindGameObjectsWithTag("drop").Length < spawnlimit)
			{
			//pickups.transform.localScale = pickups.transform.localScale * Random.Range(1,3);
			Instantiate(pickups, new Vector3(Random.Range(Minspawnrangex,Maxspawnrangex), Random.Range(Minspawnrangey,Maxspawnrangey), -1), Quaternion.identity);
			}
			time = 0;
		}
	}
}
